# Dependency for [exemplr](https://bitbucket.org/d-sci/exemplr)
This package only contains one R function, which simply generates a message
as proof that it can be called. The purpose of the package is to demonstrate
how to use a remote (bitbucket) dependency in another package.